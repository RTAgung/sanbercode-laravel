<?php
require("animal.php");
require("Frog.php");
require("Ape.php");

$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo $sheep->legs; // 2
echo $sheep->cold_blooded; // false

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())