NOMOR 1__________________________________________________________

create database myshop;

NOMOR 2__________________________________________________________

CREATE TABLE categories (
  id int AUTO_INCREMENT PRIMARY KEY,
  name varchar(255)
);

CREATE TABLE items (
  id int AUTO_INCREMENT PRIMARY KEY,
  name varchar(255),
  description varchar(255),
  price int,
  stock int,
  category_id int,
  FOREIGN KEY (category_id) REFERENCES categories(id)
);

CREATE TABLE users (
  id int AUTO_INCREMENT PRIMARY KEY,
  name varchar(255),
  email varchar(255),
  password varchar(255)
);

NOMOR 3__________________________________________________________

INSERT INTO categories (name) VALUES
('gadget'),
('cloth'),
('men'),
('women'),
('branded');

INSERT INTO items (name, description, price, stock, category_id) VALUES
('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);

INSERT INTO users (name, email, password) VALUES
('John Doe', 'john@doe.com', 'john123'),
('Jane Doe', 'jane@doe.com', 'jenita123');

NOMOR 4__________________________________________________________

A._____
SELECT id, name, password FROM users;

B._____
SELECT * FROM items WHERE price > 1000000;
SELECT * FROM items WHERE name LIKE '%sang%';

C._____
SELECT i.name, i.description, i.price, i.stock, i.category_id, c.name FROM items i JOIN categories c ON i.category_id = c.id;

NOMOR 5__________________________________________________________

UPDATE items SET price = 2500000 WHERE name = 'Sumsang b50';