@extends('template.master')

@section('content')
<div class="card p-4">
  <div class="row">
    <div class="col-3"></div>
    <div class="col-6">
      <div class="mb-4 text-center">
        <h1 class="mb-4">{{ $item->judul }}</h1>
      </div>
    </div>
    <div class="col-3 d-flex justify-content-end">
      <form action="{{ route('pertanyaan.like', ['pertanyaan' => $item->id]) }}" method="POST">
        @csrf
        @if ($like == 1)
          <input type="submit" class="btn btn-primary" name='like' value="Like">
        @else
          <input type="submit" class="btn btn-outline-primary" name='like' value="Like">
        @endif
      </form>
    </div>
  </div>
  <p>{!! $item->isi !!}</p>
  <div class="text-muted mt-4">
    <small>Author     : {{ $item->author->name }}</small>
    <br>
    <small>Updated at : {{ $item->tanggal_diperbaharui }}</small>
    <br>
    <small>Created at : {{ $item->tanggal_dibuat }}</small>
  </div>
</div>
@endsection