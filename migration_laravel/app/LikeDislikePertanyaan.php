<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeDislikePertanyaan extends Model
{
    protected $table = 'like_dislike_pertanyaan';
    protected $guarded = [];
}
