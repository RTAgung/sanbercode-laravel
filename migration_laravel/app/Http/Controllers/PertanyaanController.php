<?php

namespace App\Http\Controllers;

use App\LikeDislikePertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Pertanyaan;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); // ->except(['index']);
    }

    public function index()
    {
        // $items = DB::table('pertanyaan')->get();

        $items = Pertanyaan::all();

        // $user = Auth::user();
        // $items = $user->pertanyaan;
        return view('pertanyaan.index', compact('items'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'isi' => 'required'
        ]);

        $currentTime = Carbon::now()->toDateTimeString();

        // $query = DB::table('pertanyaan')->insert([
        //     'judul' => $request['judul'],
        //     'isi' => $request['isi'],
        //     'tanggal_dibuat' => $currentTime,
        //     'tanggal_diperbaharui' => $currentTime
        // ]);

        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request['judul'];
        // $pertanyaan->isi = $request['isi'];
        // $pertanyaan->tanggal_dibuat = $currentTime;
        // $pertanyaan->tanggal_diperbaharui = $currentTime;
        // $pertanyaan->save();

        // $pertanyaan = Pertanyaan::create([
        //     'judul' => $request['judul'],
        //     'isi' => $request['isi'],
        //     'tanggal_dibuat' => $currentTime,
        //     'tanggal_diperbaharui' => $currentTime,
        //     'profil_id' => Auth::user()->id
        // ]);

        $user = Auth::user();

        $pertanyaan = $user->pertanyaan()->create([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'tanggal_dibuat' => $currentTime,
            'tanggal_diperbaharui' => $currentTime,
        ]);

        Alert::success('Berhasil', 'Berhasil Main');
        return redirect('/pertanyaan')->with('success', 'Pertanyaan telah ditambahkan');
    }

    public function show($id)
    {
        // $item = DB::table('pertanyaan')->where('id', $id)->first();
        $item = Pertanyaan::find($id);
        $like = 0;
        if (LikeDislikePertanyaan::where([['pertanyaan_id', $id], ['profil_id', Auth::user()->id]])->first()){
            $like = 1;
        }
        return view('pertanyaan.detail', compact('item'))->with('like', $like);
    }

    public function edit($id)
    {
        // $item = DB::table('pertanyaan')->where('id', $id)->first();
        $item = Pertanyaan::find($id);

        return view('pertanyaan.edit', compact('item'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'isi' => 'required'
        ]);

        $currentTime = Carbon::now()->toDateTimeString();

        // $query = DB::table('pertanyaan')
        //     ->where('id', $id)
        //     ->update([
        //         'judul' => $request['judul'],
        //         'isi' => $request['isi'],
        //         'tanggal_diperbaharui' => $currentTime
        //     ]);

        $pertanyaan = Pertanyaan::where('id', $id)
            ->update([
                'judul' => $request['judul'],
                'isi' => $request['isi'],
                'tanggal_diperbaharui' => $currentTime
            ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan telah diperbaharui');
    }

    public function destroy($id)
    {
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        Pertanyaan::destroy($id);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan telah dihapus');
    }

    public function like($id)
    {
        $user = Auth::user();
        
        if (LikeDislikePertanyaan::where([['pertanyaan_id', $id], ['profil_id', Auth::user()->id]])->first()){
            LikeDislikePertanyaan::where([['pertanyaan_id', $id], ['profil_id', Auth::user()->id]])->delete();
        } else {
            $user->likeDislike()->attach([$id => ['poin' => 1]]);
        }

        return redirect("/pertanyaan");
    }
}
