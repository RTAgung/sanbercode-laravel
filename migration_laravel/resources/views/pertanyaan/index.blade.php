@extends('template.master')

@section('content')
  <div class="card p-4">
    <div class="row">
      <div class="col-3"></div>
      <div class="col-6">
        <div class="mb-4 text-center">
          <h1 class="mb-4">Daftar Pertanyaan</h1>
          @if (session('success'))
            <div class="alert alert-success">
              {{ session('success') }}
            </div>
          @endif
          <a href="{{ route('pertanyaan.create') }}" class="btn btn-primary">Tambah Pertanyaan</a>
        </div>
      </div>
      <div class="col-3">
        @guest
          <div class="d-flex justify-content-end">
            <a class="btn btn-secondary mr-2" href="/login">Login</a>
            <a class="btn btn-outline-secondary" href="/register">Register</a>
          </div>
        @else
          <div class="d-flex justify-content-end">
            <a id="navbarDropdown" class="btn btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              Halo, {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </div>
          </div>
        @endguest
      </div>
    </div>
    <div class="row">
      @forelse ($items as $key => $item)
        <div class="col-sm-6 mb-4">
          <div class="card text-center">
            <div class="card-header">
              <h5 class="card-title">{{ $item->judul }}</h5>
            </div>
            <div class="card-body">
              <p class="card-text">{!! $item->isi !!}</p> 
            </div>
            <div class="card-footer text-muted" style="justify-content: center; display:flex;">
              <a href="{{ route('pertanyaan.show', ['pertanyaan' => $item->id]) }}" class="btn btn-info mr-2">Show</a>
              <a href="/pertanyaan/{{ $item->id }}/edit" class="btn btn-warning mr-2">Edit</a>
              <form action="/pertanyaan/{{ $item->id }}" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" value="delete" class="btn btn-danger">
              </form>
            </div>
          </div>
        </div>
      @empty
        <div class="col-12 text-center">
          <p>tidak ada pertanyaan</p>
        </div>
      @endforelse
    </div>
  </div>
@endsection