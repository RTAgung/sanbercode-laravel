<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>SanberBook</title>
</head>
<body>
  <h1>Buat Account Baru!</h1>
  <h3>Sign Up Form</h3>
  <form action="/welcome" method="POST">
    @csrf
    <p>First name:</p>
    <input type="text" name="first_name">
    <p>Last name:</p>
    <input type="text" name="last_name">
    <p>Gander: </p>
    <input type="radio" id="male" name="gender">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender">
    <label for="other">Other</label><br>
    <p>Nationality</p>
    <select>
      <option selected>Indonesian</option>
      <option>Japan</option>
      <option>English</option>
    </select>
    <p>Language Spoken:</p>
    <input type="checkbox" id="indonesia">
    <label for="indonesia">Bahasa Indonesia</label><br>
    <input type="checkbox" id="english">
    <label for="english">English</label><br>
    <input type="checkbox" id="checkother">
    <label for="checkother">Other</label><br>
    <p>Bio:</p>
    <textarea rows="5" cols="25"></textarea><br>
    <input type="submit" value="Sign Up">
  </form>
</body>
</html>