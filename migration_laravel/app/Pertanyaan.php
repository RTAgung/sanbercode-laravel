<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan";
    protected $fillable = ['judul', 'isi', 'tanggal_dibuat', 'tanggal_diperbaharui', 'profil_id'];
    protected $guarded = [];

    public function author(){
        return $this->belongsTo('App\User', 'profil_id');
    }

    public function likeDislike(){
        return $this->belongsToMany('App\User', 'like_dislike_pertanyaan', 'pertanyaan_id', 'profil_id')
            ->withPivot('poin')
            ->withTimestamps();
    }
}
